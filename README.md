# VGG OAuth2 JWT Prototype

## Resource Server

Resource server can be launched locally from **resourceserver** folder by issuing the command:

```cmd
mvn spring-boot:run
```

By default it is configured to run on **9000** port.

Resource server also contains the following mocked data:

* **User** domain object
* **Tenancy** domain object
* **Organizations** domain object

All of the above domain objects have theirs services and controllers, so that the data can be accessed via REST API.
Each domain object has its own *.read permission, which defines access to it. This means that the one resources can be accessed by client only if the proper scope is set.

> Note: Resource Server doesn't perform any token decoding as he is configured to check token on a provided token check endpoint (**security.oauth2.resource.token-info-uri**)

## Authorization Server

Cloudfoundry is used as an authorization server. Its configuration can be found @ /uaa/uaa.yml.

To launch CloudFoundry using configuration file, the path to this file can be provided as follows:

```cmd
CLOUD_FOUNDRY_CONFIG_PATH=<path to folder containing config file> ./gradlew run
```

Authorization Server is configured to use a locally deployed PostgreSQL database. In case if it is not available an in-memory database will be used.

### Scopes

The following scopes are defined:

* sensitive.resource - identifies a resource that is sensitive
* sensitive.users.read - read **User** domain object permission
* sensitive.tenancies.read - read **Tenancy** domain object permission
* sensitive.organizations.read - read **Organization** domain object permission

### Client Credentials Grant Type

A **client_credentials** grant type is working for resource server and can be checked using curl:

* request token:
```curl
curl 'http://localhost:8080/uaa/oauth/token' -i -u 'resourceserver:resourceserversecret' -X POST -H 'Accept: application/json' -H 'Content-Type: application/x-www-form-urlencoded' -d 'grant_type=client_credentials&response_type=token'
```

* sample response:
```json
{"access_token":"eyJhbGciOiJIUzI1NiIsImtpZCI6ImxlZ2FjeS10b2tlbi1rZXkiLCJ0eXAiOiJKV1QifQ.eyJqdGkiOiJhZDA1YTU2ZTYzMWU0YWNhOWJmYzRkMjY0ZGQ5MzJhOCIsInN1YiI6InJlc291cmNlc2VydmVyIiwiYXV0aG9yaXRpZXMiOlsic2Vuc2l0aXZlLm9yZ2FuaXphdGlvbnMucmVhZCIsInBhc3N3b3JkLndyaXRlIiwidWFhLnJlc291cmNlIiwic2Vuc2l0aXZlLnJlc291cmNlIiwic2NpbS53cml0ZSIsInNjaW0ucmVhZCIsInNlbnNpdGl2ZS50ZW5hbmNpZXMucmVhZCIsInNlbnNpdGl2ZS51c2Vycy5yZWFkIl0sInNjb3BlIjpbInNlbnNpdGl2ZS5vcmdhbml6YXRpb25zLnJlYWQiLCJwYXNzd29yZC53cml0ZSIsInVhYS5yZXNvdXJjZSIsInNlbnNpdGl2ZS5yZXNvdXJjZSIsInNjaW0ud3JpdGUiLCJzY2ltLnJlYWQiLCJzZW5zaXRpdmUudGVuYW5jaWVzLnJlYWQiLCJzZW5zaXRpdmUudXNlcnMucmVhZCJdLCJjbGllbnRfaWQiOiJyZXNvdXJjZXNlcnZlciIsImNpZCI6InJlc291cmNlc2VydmVyIiwiYXpwIjoicmVzb3VyY2VzZXJ2ZXIiLCJncmFudF90eXBlIjoiY2xpZW50X2NyZWRlbnRpYWxzIiwicmV2X3NpZyI6IjRlNjhhMjI3IiwiaWF0IjoxNDgwNTE5MTA5LCJleHAiOjE0ODA1NjIzMDksImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC91YWEvb2F1dGgvdG9rZW4iLCJ6aWQiOiJ1YWEiLCJhdWQiOlsic2NpbSIsInBhc3N3b3JkIiwicmVzb3VyY2VzZXJ2ZXIiLCJ1YWEiLCJzZW5zaXRpdmUudGVuYW5jaWVzIiwic2Vuc2l0aXZlLnVzZXJzIiwic2Vuc2l0aXZlLm9yZ2FuaXphdGlvbnMiLCJzZW5zaXRpdmUiXX0.bedluN4iSsC851Q0jNOUNiFSIH9TErMRoldYb3apCrE","token_type":"bearer","expires_in":43199,"scope":"sensitive.organizations.read password.write uaa.resource sensitive.resource scim.write scim.read sensitive.tenancies.read sensitive.users.read","jti":"ad05a56e631e4aca9bfc4d264dd932a8"}
```

* Request resource:
```curl
curl -i 'http://localhost:9000/organizations' -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsImtpZCI6ImxlZ2FjeS10b2tlbi1rZXkiLCJ0eXAiOiJKV1QifQ.eyJqdGkiOiJhZDA1YTU2ZTYzMWU0YWNhOWJmYzRkMjY0ZGQ5MzJhOCIsInN1YiI6InJlc291cmNlc2VydmVyIiwiYXV0aG9yaXRpZXMiOlsic2Vuc2l0aXZlLm9yZ2FuaXphdGlvbnMucmVhZCIsInBhc3N3b3JkLndyaXRlIiwidWFhLnJlc291cmNlIiwic2Vuc2l0aXZlLnJlc291cmNlIiwic2NpbS53cml0ZSIsInNjaW0ucmVhZCIsInNlbnNpdGl2ZS50ZW5hbmNpZXMucmVhZCIsInNlbnNpdGl2ZS51c2Vycy5yZWFkIl0sInNjb3BlIjpbInNlbnNpdGl2ZS5vcmdhbml6YXRpb25zLnJlYWQiLCJwYXNzd29yZC53cml0ZSIsInVhYS5yZXNvdXJjZSIsInNlbnNpdGl2ZS5yZXNvdXJjZSIsInNjaW0ud3JpdGUiLCJzY2ltLnJlYWQiLCJzZW5zaXRpdmUudGVuYW5jaWVzLnJlYWQiLCJzZW5zaXRpdmUudXNlcnMucmVhZCJdLCJjbGllbnRfaWQiOiJyZXNvdXJjZXNlcnZlciIsImNpZCI6InJlc291cmNlc2VydmVyIiwiYXpwIjoicmVzb3VyY2VzZXJ2ZXIiLCJncmFudF90eXBlIjoiY2xpZW50X2NyZWRlbnRpYWxzIiwicmV2X3NpZyI6IjRlNjhhMjI3IiwiaWF0IjoxNDgwNTE5MTA5LCJleHAiOjE0ODA1NjIzMDksImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC91YWEvb2F1dGgvdG9rZW4iLCJ6aWQiOiJ1YWEiLCJhdWQiOlsic2NpbSIsInBhc3N3b3JkIiwicmVzb3VyY2VzZXJ2ZXIiLCJ1YWEiLCJzZW5zaXRpdmUudGVuYW5jaWVzIiwic2Vuc2l0aXZlLnVzZXJzIiwic2Vuc2l0aXZlLm9yZ2FuaXphdGlvbnMiLCJzZW5zaXRpdmUiXX0.bedluN4iSsC851Q0jNOUNiFSIH9TErMRoldYb3apCrE'
```

* sample response
```json
[{"name":"ABC Organization","users":[{"name":"John Smith"},{"name":"Amy Lee"},{"name":"Sarah Paulson"}],"tenancies":[{"info":"TenancyA"},{"info":"TenancyB"},{"info":"TenancyC"}]}]
```

### Web Client Application

Web client application is used for **authorization_code** grant type. It can be started from the **webclient** folder by issuing the command:

```cmd
mvn spring-boot:run
```

By default it is configured to run on **3000** port.

The flow can be checked via browser. Endpoint is **http://localhost:3000**
Once opened, there would be references for:

* Users
* Tenancies
* Organizations

After clicking the reference the user-agent will be redirected to authenticate @ /login endpoint, which is running on UAA by default.
After submitting the correct credentials, the user will be prompted to provide the necessary permissions.
Each *sensitive* permission corresponds to a particular resource, (e.g. **sensitive.organizations.read** for **Organizations**).
After permissions are provided, in case if client access is granted, the user-agent will be redirected to a resource server.

### Implicit Grant Type

An **implicit** grant type is working and can be checked using curl:

* request token:
```curl
curl 'http://localhost:8080/uaa/oauth/authorize?response_type=token&client_id=resourceserver_shell&scope=sensitive.resource,sensitive.organizations.read&redirect_uri=http%3A%2F%2Flocalhost%3A4000' -i -H 'Accept: application/x-www-form-urlencoded'
```

A user-agent will be redirected back to an uri, specified as **redirect_uri**, which is (*http://localhost:4000*) with an appropriate token:

```
Location: http://localhost:4000?token_type=bearer&access_token=eyJhbGciOiJIUzI1NiIsImtpZCI6ImxlZ2FjeS10b2tlbi1rZXkiLCJ0eXAiOiJKV1QifQ.eyJqdGkiOiI4ZTczM2NlMDM4YzM0MmUxYTU4MjIxNTVlMzc5OTQ4OSIsInN1YiI6IjdmZDJkMDI2LTI3MDQtNDkyMi04MDhhLWVlOGJkYWFjZGQyNyIsInNjb3BlIjpbIm9wZW5pZCJdLCJjbGllbnRfaWQiOiJhcHAiLCJjaWQiOiJhcHAiLCJhenAiOiJhcHAiLCJ1c2VyX2lkIjoiN2ZkMmQwMjYtMjcwNC00OTIyLTgwOGEtZWU4YmRhYWNkZDI3Iiwib3JpZ2luIjoidWFhIiwidXNlcl9uYW1lIjoibWFyaXNzYSIsImVtYWlsIjoibWFyaXNzYUB0ZXN0Lm9yZyIsInJldl9zaWciOiI5MTc2Mzc1NSIsImlhdCI6MTQ3NDkyMzQ3OSwiZXhwIjoxNDc0OTY2Njc5LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvdWFhL29hdXRoL3Rva2VuIiwiemlkIjoidWFhIiwiYXVkIjpbImFwcCIsIm9wZW5pZCJdfQ.awDtiqabc7Q88nqjwEMFO1y1CSP_nl5gsHyF1vkdj6Q&expires_in=43199&jti=8e733ce038c342e1a5822155e3799489
```
