package com.vgg.oauth2.prototype.resourceserver.organization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * @author shell
 * @version 1.0.0
 * @since 1.0.0
 */
@RestController
@RequestMapping("/organizations")
public class OrganizationController {

	private final OrganizationService organizationService;

	@Autowired
	public OrganizationController(OrganizationService organizationService) {
		this.organizationService = organizationService;
	}

	@RequestMapping(method = RequestMethod.GET)
	Set<Organization> organizations() {
		return organizationService.findAll();
	}

}
