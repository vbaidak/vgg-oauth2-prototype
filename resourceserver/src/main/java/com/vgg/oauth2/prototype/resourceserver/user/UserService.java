package com.vgg.oauth2.prototype.resourceserver.user;

import java.util.Set;

/**
 * @author shell
 * @version 1.0.0
 * @since 1.0.0
 */
public interface UserService {

	Set<User> findAll();

}
