package com.vgg.oauth2.prototype.resourceserver.tenancy;

import java.io.Serializable;

/**
 * @author shell
 * @version 1.0.0
 * @since 1.0.0
 */
public class Tenancy implements Serializable {

	private String info;

	public Tenancy(String info) {
		this.info = info;
	}

	public Tenancy() {
	}

	public String getInfo() {
		return info;
	}

	@Override
	public String toString() {
		return "Tenancy{" +
				"info='" + info + '\'' +
				'}';
	}

}
