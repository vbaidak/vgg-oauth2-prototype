package com.vgg.oauth2.prototype.resourceserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shell
 * @version 1.0.0
 * @since 1.0.0
 */
@SpringBootApplication
public class ResourceServerApp {

	public static void main(String[] args) {
		SpringApplication.run(ResourceServerApp.class, args);
	}

}
