package com.vgg.oauth2.prototype.resourceserver.organization;

import com.vgg.oauth2.prototype.resourceserver.tenancy.Tenancy;
import com.vgg.oauth2.prototype.resourceserver.tenancy.TenancyService;
import com.vgg.oauth2.prototype.resourceserver.user.User;
import com.vgg.oauth2.prototype.resourceserver.user.UserService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author shell
 * @version 1.0.0
 * @since 1.0.0
 */
@Service
public class OrganizationServiceImpl implements OrganizationService, InitializingBean {

	private final UserService userService;

	private final TenancyService tenancyService;

	private Set<Organization> organizations = new LinkedHashSet<>();

	@Autowired
	public OrganizationServiceImpl(UserService userService, TenancyService tenancyService) {
		this.userService = userService;
		this.tenancyService = tenancyService;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Set<User> users = userService.findAll();
		Set<Tenancy> tenancies = tenancyService.findAll();
		Organization organization = new Organization("ABC Organization", users, tenancies);
		organizations.add(organization);
	}

	public Set<Organization> findAll() {
		return organizations;
	}
}
