package com.vgg.oauth2.prototype.resourceserver.tenancy;

import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author shell
 * @version 1.0.0
 * @since 1.0.0
 */
@Service
public class TenancyServiceImpl implements TenancyService {

	private final Set<Tenancy> tenancies = new LinkedHashSet<>();

	public TenancyServiceImpl() {
		tenancies.add(new Tenancy("TenancyA"));
		tenancies.add(new Tenancy("TenancyB"));
		tenancies.add(new Tenancy("TenancyC"));
	}

	public Set<Tenancy> findAll() {
		return tenancies;
	}

}
