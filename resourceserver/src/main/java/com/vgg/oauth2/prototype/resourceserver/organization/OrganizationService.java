package com.vgg.oauth2.prototype.resourceserver.organization;

import java.util.Set;

/**
 * @author shell
 * @version 1.0.0
 * @since 1.0.0
 */
public interface OrganizationService {

	Set<Organization> findAll();

}
