package com.vgg.oauth2.prototype.resourceserver.tenancy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * @author shell
 * @version 1.0.0
 * @since 1.0.0
 */
@RestController
@RequestMapping("/tenancies")
public class TenancyController {

	private final TenancyService tenancyService;

	@Autowired
	public TenancyController(TenancyService tenancyService) {
		this.tenancyService = tenancyService;
	}

	@RequestMapping(method = RequestMethod.GET)
	Set<Tenancy> tenancies() {
		return tenancyService.findAll();
	}

}
