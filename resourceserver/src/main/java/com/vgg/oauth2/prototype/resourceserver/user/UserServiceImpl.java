package com.vgg.oauth2.prototype.resourceserver.user;

import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author shell
 * @version 1.0.0
 * @since 1.0.0
 */
@Service
public class UserServiceImpl implements UserService {

	private final Set<User> users = new LinkedHashSet<>();

	public UserServiceImpl() {
		users.add(new User("John Smith"));
		users.add(new User("Amy Lee"));
		users.add(new User("Sarah Paulson"));
	}

	@Override
	public Set<User> findAll() {
		return users;
	}

}
