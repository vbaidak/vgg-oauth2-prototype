package com.vgg.oauth2.prototype.resourceserver;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

/**
 * @author shell
 * @version 1.0.0
 * @since 1.0.0
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig implements ResourceServerConfigurer {

	@Value("${security.oauth2.resource.id}")
	private String resourceId;

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.resourceId(resourceId);
	}

	@Override
	public void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.authorizeRequests()
				.antMatchers(HttpMethod.GET, "/organizations/**").access("#oauth2.hasScope('sensitive.organizations.read')")
				.antMatchers(HttpMethod.GET, "/tenancies/**").access("#oauth2.hasScope('sensitive.tenancies.read')")
				.antMatchers(HttpMethod.GET, "/users/**").access("#oauth2.hasScope('sensitive.users.read')");
	}

}
