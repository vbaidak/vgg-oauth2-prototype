package com.vgg.oauth2.prototype.resourceserver.tenancy;

import java.util.Set;

/**
 * @author shell
 * @version 1.0.0
 * @since 1.0.0
 */
public interface TenancyService {

	Set<Tenancy> findAll();

}
