package com.vgg.oauth2.prototype.resourceserver.organization;

import com.vgg.oauth2.prototype.resourceserver.tenancy.Tenancy;
import com.vgg.oauth2.prototype.resourceserver.user.User;

import java.io.Serializable;
import java.util.Set;

/**
 * @author shell
 * @version 1.0.0
 * @since 1.0.0
 */
public class Organization implements Serializable {

	private String name;

	private Set<User> users;

	private Set<Tenancy> tenancies;

	public Organization(String name, Set<User> users, Set<Tenancy> tenancies) {
		this.name = name;
		this.users = users;
		this.tenancies = tenancies;
	}

	public Organization() {
	}

	public String getName() {
		return name;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void addUser(User user) {
		this.users.add(user);
	}

	public Set<Tenancy> getTenancies() {
		return tenancies;
	}

	public void addTenancy(Tenancy tenancy) {
		this.tenancies.add(tenancy);
	}

	@Override
	public String toString() {
		return "Organization{" +
				"name='" + name + '\'' +
				", users=" + users +
				", tenancies=" + tenancies +
				'}';
	}

}
