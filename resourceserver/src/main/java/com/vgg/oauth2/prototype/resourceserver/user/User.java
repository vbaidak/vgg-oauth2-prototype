package com.vgg.oauth2.prototype.resourceserver.user;

import java.io.Serializable;

/**
 * @author shell
 * @version 1.0.0
 * @since 1.0.0
 */
public class User implements Serializable {

	private String name;

	public User(String name) {
		this.name = name;
	}

	public User() {
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "User{" +
				"name='" + name + '\'' +
				'}';
	}

}
