package com.vgg.oauth2.prototype.webclient;

import com.vgg.oauth2.prototype.resourceserver.organization.Organization;
import com.vgg.oauth2.prototype.resourceserver.tenancy.Tenancy;
import com.vgg.oauth2.prototype.resourceserver.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

/**
 * @author shell
 * @version 1.0.0
 * @since 1.0.0
 */
@RestController
@PropertySource("classpath:client.properties")
public class WebClientController {

	private final OAuth2RestTemplate restTemplate;

	@Value("${resource.server.url}")
	private String resourceServerUrl;

	@Autowired
	public WebClientController(OAuth2RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@RequestMapping("/organizations")
	public Collection<Organization> organizations() {
		return requestResource("/organizations");
	}

	@RequestMapping("/tenancies")
	public Collection<Tenancy> tenancies() {
		return requestResource("/tenancies");
	}

	@RequestMapping("/users")
	public Collection<User> users() {
		return requestResource("/users");
	}

	private <T> T requestResource(String path) {
		String url = resourceServerUrl + path;
		return restTemplate.exchange(
				url,
				HttpMethod.GET,
				null,
				new ParameterizedTypeReference<T>() { }
		).getBody();
	}

}
